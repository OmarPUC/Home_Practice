package com.example.omarsharif.homepractice;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class TextView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_view);
    }

    public void goToNextActivity(View view) {
        Intent intent = new Intent(TextView.this, TextView2.class);
        startActivity(intent);
    }
}
